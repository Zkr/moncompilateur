#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <map>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {INTEGER, BOOLEAN};


TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

map<string, enum TYPES> DeclaredVariables;

unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
TYPES Identifier(void){
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return INTEGER;
}

TYPES Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return INTEGER;
}

TYPES Expression(void);			// Called by Term() and calls Term()
void Statement(void);
TYPES AssignementStatement(void);

TYPES IfStatement(void){
	TYPES type8;
	if(strcmp(lexer->YYText(), "If" )==0){
		current=(TOKEN) lexer->yylex();
		type8 = Expression();
		if (type8 != BOOLEAN){
			Error("l'expression n'est pas de type BOOLEAN");
		}
		cout << "\t pop %rax "<<endl;
		cout << "\t cmpq $0, %rax "<<endl;
		cout << "\t je ElseIF "<<endl;
		cout << "\t ThenIF: "<<endl;
		if(strcmp(lexer->YYText(), "Then" )==0){
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\t jmp SuiteIF "<<endl;
			cout << "\t ElseIF: "<<endl;
			if(strcmp(lexer->YYText(), "Else" )==0){
				current=(TOKEN) lexer->yylex();
				Statement();
			}
		}
		else{
			Error("Then attendu");
		}
	}
	else{
		Error("If attendu");
	}
	cout << "\t SuiteIF:"<<endl;
}

TYPES WhileStatement(void){
	TYPES type9;
	if(strcmp(lexer->YYText(), "While" )==0){
		current=(TOKEN) lexer->yylex();
		type9=Expression();
		if (type9!=BOOLEAN){
			Error("l'expression n'est pas de type BOOLEAN");
		}
		cout << "\t pop %rax "<<endl;
		cout << "\t cmpq $0, %rax "<<endl;
		cout << "\t je SuiteWHILE "<<endl;
		cout << "\t DoWHILE: "<<endl;
		if(strcmp(lexer->YYText(), "Do" )==0){
		current=(TOKEN) lexer->yylex();
		Statement();
		}
		else{
			Error("Do attendu");
		}
	}
	else{
		Error("While attendu");
	}
	cout << "\t SuiteWHILE:"<<endl;
}

void ForStatement(void){
	if(strcmp(lexer->YYText(), "For" )==0){
		current=(TOKEN) lexer->yylex();
		AssignementStatement();
		if(strcmp(lexer->YYText(), "To" )==0){
			current=(TOKEN) lexer->yylex();
			cout << "\t toDO: "<<endl;
			Expression();
			cout << "\t pop %rax "<<endl;
			cout << "\t cmpq $0, %rax "<<endl;
			cout << "\t je SuiteFOR "<<endl;
			if(strcmp(lexer->YYText(), "Do" )==0){
				current=(TOKEN) lexer->yylex();
				Statement();
				cout << "\t addq $1, %rax"<<endl;
				cout << "\t jmp toDO"<<endl;
				
			}
			else{
				Error("Do attendu");
			}
		}
		else{
		 Error("To attendu");
		}
	}
	else{
		Error("For attendu");
	}
	cout << "\t SuiteFOR:"<<endl;
}

void BlockStatement(void){
	current=(TOKEN) lexer->yylex();
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	};
	if(current!=KEYWORD||strcmp(lexer->YYText(), "End")!=0){
		Error("End attendu");
	}
	current=(TOKEN) lexer->yylex();  
}


TYPES Factor(void){
	TYPES type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type=Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else{
		if (current==NUMBER){
			type=Number();
		}
		else{
			if(current==ID){
				type=Identifier();
			}
			else{
				Error("'(' ou chiffre ou lettre attendue");
			}
		}
	}
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPES Term(void){
	TYPES type1, type2;
	OPMUL mulop;
	type1=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2=Factor();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				if (type2!=BOOLEAN){
					Error("Pas le même type");
				}
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if (type2!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				if (type2!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				if (type2!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPES SimpleExpression(void){
	TYPES type3, type4;
	OPADD adop;
	type3=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		type4=Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				if (type4!=BOOLEAN){
					Error("Pas le même type");
				}
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				if (type4!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:
				if (type4!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return type3;
}

// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables[lexer->YYText()]=INTEGER;
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables[lexer->YYText()]=INTEGER; 
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPES Expression(void){
	TYPES type5;
	OPREL oprel;
	type5=SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		type5=SimpleExpression();
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				if (type5!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				if (type5!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				if (type5!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				if (type5!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				if (type5!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				if (type5!=INTEGER){
					Error("Pas le même type");
				}
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		type5=BOOLEAN;
	}
	return type5;
}

// AssignementStatement := Identifier ":=" Expression
TYPES AssignementStatement(void){
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	TYPES type6 = DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	TYPES type7 = Expression();
	cout << "\tpop "<<variable<<endl;
	if (type6!=type7){
		Error("pas du même type");
	}
}

// Statement := AssignementStatement
void Statement(void){
	if(strcmp(lexer->YYText(), "If" )==0){
		IfStatement();
	}
	else if(strcmp(lexer->YYText(), "While" )==0){
		WhileStatement(); 
	}
	else if(strcmp(lexer->YYText(), "For" )==0){
		ForStatement(); 
	}
	else if(strcmp(lexer->YYText(),"Begin")==0){
			BlockStatement();
	}

	else{
		AssignementStatement();
	}
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





